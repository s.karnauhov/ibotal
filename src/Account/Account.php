<?php

namespace Picalytics\Ibotal\Account;

class Account {
    
    /**
     *
     * @var string
     */
    private $login;
    
    /**
     *
     * @var string
     */
    private $pass;
    
    /**
     *
     * @var Email\BaseAccountEmail
     */
    private $email;


    /**
     * 
     * @param string $login
     * @param string $pass
     * @param \Picalytics\Ibotal\Account\Email\BaseAccountEmail $email
     */
    public function __construct(string $login = null, string $pass = null, Email\BaseAccountEmail $email = null) {
        $this->login = $login;
        $this->pass = $pass;
        $this->email = $email;
    }
    
    /**
     * 
     * @param string $login
     * @return \Picalytics\Ibotal\Bot\Bot
     */
    public function setLogin(string $login): Bot {
        $this->login = $login;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getLogin(): string {
        return $this->login;
    }
    
    /**
     * 
     * @param string $pass
     * @return \Picalytics\Ibotal\Bot\Bot
     */
    public function setPass(string $pass): Bot {
        $this->pass = $pass;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getPass(): string {
        return $this->pass;
    }
    
    /**
     * 
     * @param \Picalytics\Ibotal\Account\Email\BaseAccountEmail $email
     * @return \Picalytics\Ibotal\Account\Bot
     */
    public function setEmail(Email\BaseAccountEmail $email): Bot {
        $this->email = $email;
        return $this;
    }
    
    /**
     * 
     * @return \Picalytics\Ibotal\Account\Email\BaseAccountEmail
     */
    public function getEmail(): Email\BaseAccountEmail {
        return $this->email;
    }
    
}
