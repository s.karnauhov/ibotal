<?php

namespace Picalytics\Ibotal\Account\Email;

abstract class BaseAccountEmail {
    
    /**
     *
     * @var string
     */
    private $email;
    
    /**
     *
     * @var string
     */
    private $pass;
    
    /**
     * 
     * @param string $email
     * @param string $pass
     */
    public function __construct(string $email = null, string $pass = null) {
        $this->email = $email;
        $this->pass = $pass;
    }
    
    /**
     * 
     * @param string $email
     * @return \Picalytics\Ibotal\Account\Email\BaseAccountEmail
     */
    public function setEmail(string $email): BaseAccountEmail {
        $this->email = $email;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }
    
    /**
     * 
     * @param string $pass
     * @return \Picalytics\Ibotal\Account\Email\BaseAccountEmail
     */
    public function setPass(string $pass): BaseAccountEmail {
        $this->pass = $pass;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getPass(): string {
        return $this->pass;
    }
    
}
