<?php

namespace Picalytics\Ibotal;

class InstagramClient extends \InstagramAPI\Instagram {
    
    /**
     *
     * @var int|null
     */
    protected $checkAccountId = null;

    /**
     * @param string $username
     * @param string $password
     */
    public function changeUser(string $username, string $password) {
        $this->_setUser($username, $password);
    }

    /**
     * 
     * @param string $checkApiPath
     * @param int $verificationType
     * @return \InstagramAPI\Response
     */
    public function sendVerificationCode(string $checkApiPath, int $verificationType): \InstagramAPI\Response {
        sleep(1);
        $customResponse = $this->request($checkApiPath)
            ->setNeedsAuth(false)
            ->addPost('choice', $verificationType)
            ->addPost('_uuid', $this->uuid)
            ->addPost('guid', $this->uuid)
            ->addPost('device_id', $this->device_id)
            ->addPost('_uid', $this->account_id)
            ->addPost('_csrftoken', $this->client->getToken())
            ->getResponse(new \InstagramAPI\Response\UserInfoResponse());
        
        $this->client->saveCookieJar();

        return $customResponse;
    }

    /**
     * 
     * @param string $checkApiPath
     * @param string $code
     * @return \InstagramAPI\Response\LoginResponse
     * @throws \RuntimeException
     */
    public function loginByVerificationCode(string $checkApiPath, string $code): \InstagramAPI\Response\LoginResponse {
        sleep(1);
        $customResponse = $this->request($checkApiPath)
            ->setNeedsAuth(false)
            ->addPost('security_code', $code)
            ->addPost('_uuid', $this->uuid)
            ->addPost('guid', $this->uuid)
            ->addPost('device_id', $this->device_id)
            ->addPost('_uid', $this->account_id)
            ->addPost('_csrftoken', $this->client->getToken())
            ->getResponse(new \InstagramAPI\Response\LoginResponse());

        if (!$customResponse instanceof \InstagramAPI\Response\LoginResponse || !$customResponse->isOk()) {
            throw new \RuntimeException('Response must be instance of \InstagramAPI\Response\LoginResponse');
        }

        $this->checkAccountId = $this->setAccountIdFromCheckpointUrl($checkApiPath);
        $this->updateLoginState($customResponse);

        $decoded = $customResponse->asArray();
        if (isset($decoded['logged_in_user'])) {
            $this->_sendLoginFlow(true, 1800);
        }

        return $customResponse;
    }

    /**
     * 
     * @param \InstagramAPI\Response\LoginResponse $response
     * @throws \InvalidArgumentException
     */
    public function updateLoginState(\InstagramAPI\Response\LoginResponse $response) {
        if (!$response->isOk()) {
            throw new \InvalidArgumentException('Invalid login response provided');
        }

        $this->isMaybeLoggedIn = true;
        $this->account_id = $this->checkAccountId ?? $response->getLoggedInUser()->getPk();
        $this->settings->set('account_id', $this->account_id);
        $this->settings->set('last_login', time());
    }

    /**
     * @param string $checkApiPath
     * @return int|null
     */
    public function setAccountIdFromCheckpointUrl(string $checkApiPath) {
        $matches = explode('/', $checkApiPath);
        $id = isset($matches[1]) ? (int)$matches[1] : null;
        return $id;
    }
    
}
