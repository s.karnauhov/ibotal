<?php

namespace Picalytics\Ibotal;

class InstagramAutoLogin {
    
    /**
     *
     * @var InstagramService
     */
    protected $instagramService;
    
    /**
     *
     * @var \Monolog\Logger
     */
    protected $logger;
    
    /**
     *
     * @var array
     */
    protected $multiThreadingOptions = [];
    
    /**
     *
     * @var array
     */
    protected $accounts = [];

    /**
     * 
     * @param array $config
     * @param string $logFile
     */
    public function __construct(array $config, string $logFile) {
        $this->instagramService = new InstagramService($config);
        $this->logger = new \Monolog\Logger('instagram-autologin', [
            new \Monolog\Handler\StreamHandler('php://stdout'),
            new \Monolog\Handler\StreamHandler($logFile),
        ]);
        $this->multiThreadingOptions = [
            'size' => pow(1024,2),
            'process' => [],
            'callback' => function() {
                
            },
        ];
    }

    /**
     * 
     * @param string $accountsData
     * @param string $delimiter
     * @throws \Exception
     */
    public function run(string $accountsData, string $delimiter = ':') {
        $accounts = explode("\n", trim($accountsData));
        preg_match_all('/^[^:]+/mi', $accountsData, $matches);
        if (count($matches) > 0) {
            $this->accounts = $matches[0];
        }
        array_walk($this->accounts, function(&$value) {
            $value = trim($value);
        });
        foreach ($accounts as $accountData) {
            $this->multiThreadingOptions['process'][] = function() use ($accountData, $delimiter) {
                try {
                    $accountInfo = array_filter(explode($delimiter, trim($accountData)));
                    if (count($accountInfo) !== 8) {
                        $errorMessage = 'Can\'t parse bot data: "' . $accountData . '"';
                        $this->logger->err($errorMessage);
                        throw new \UnexpectedValueException($errorMessage);
                    }
                    list(
                            $accountLogin,
                            $accountPass,
                            $accountEmail,
                            $accountEmailPass,
                            $proxyHost,
                            $proxyPort,
                            $proxyLogin,
                            $proxyPass,
                    ) = $accountInfo;

                    $this->logger->info('Try to login ' . $accountLogin);
                    $proxy = new \Picalytics\Ibotal\Proxy\Proxy($proxyHost, $proxyPort, $proxyLogin, $proxyPass);
                    $email = new \Picalytics\Ibotal\Account\Email\MailRuAccountEmail($accountEmail, $accountEmailPass);
                    $account = new \Picalytics\Ibotal\Account\Account($accountLogin, $accountPass, $email);
                    $this->instagramService->setProxy($proxy);
                    $this->instagramService->setAccount($account);

                    $tryToLoginResult = $this->instagramService->tryToLogin();
                    if ($tryToLoginResult->status()) {
                        $this->logger->info($this->instagramService->getAccount()->getLogin() . ' logged successfully');
                        return $this->instagramService->getAccount()->getLogin();
                    }
                    if ($tryToLoginResult->getError() === Result::CHALLENGE_REQUIRED_ERROR) {
                        sleep(60);
                        if ($this->instagramService->challengeRequiredLogin($tryToLoginResult->getData()['challengeRequiredUrl'])) {
                            $this->logger->info($this->instagramService->getAccount()->getLogin() . ' logged successfully after challenge required');
                            return $this->instagramService->getAccount()->getLogin();
                        }
                    }

                    $this->logger->err($this->instagramService->getAccount()->getLogin() . ' logging failed');
                    return;
                } catch (\Exception $e) {
                    return;
                }
            };
        }
        $loggedAccounts = $this->multiThreading();
        return array_filter($this->accounts, function($account) use ($loggedAccounts) {
            foreach ($loggedAccounts as $loggedAccount) {
                if (strpos($loggedAccount, $account) === 0) {
                    return true;
                }
            }
            return false;
        });
    }
    
    /**
     * 
     * @return void
     */
    protected function multiThreading() {
        $sharedMemoryMonitor = shmop_open(ftok(__FILE__, chr(0)), "c", 0644, count($this->multiThreadingOptions['process']));
        $sharedMemoryIds = (object) [];
        for ($i = 1; $i <= count($this->multiThreadingOptions['process']); $i++) {
            $sharedMemoryIds->$i = shmop_open(ftok(__FILE__, chr($i)), "c", 0644, $this->multiThreadingOptions['size']);
        }
        for ($i = 1; $i <= count($this->multiThreadingOptions['process']); $i++) {
            $pid = pcntl_fork();
            if (!$pid) {
                if ($i == 1) {
                    usleep(100000);
                }
                $sharedMemoryData = $this->multiThreadingOptions['process'][$i - 1]();
                shmop_write($sharedMemoryIds->$i, $sharedMemoryData, 0);
                shmop_write($sharedMemoryMonitor, "1", $i - 1);
                exit($i);
            }
        }
        while (pcntl_waitpid(0, $status) != -1) {
            if (shmop_read($sharedMemoryMonitor, 0, count($this->multiThreadingOptions['process'])) == str_repeat("1", count($this->multiThreadingOptions['process']))) {
                $result = [];
                foreach ($sharedMemoryIds as $key => $value) {
                    $result[$key - 1] = shmop_read($sharedMemoryIds->$key, 0, $this->multiThreadingOptions['size']);
                    shmop_delete($sharedMemoryIds->$key);
                }
                shmop_delete($sharedMemoryMonitor);
                $this->multiThreadingOptions['callback']($result);
            }
        }
        return $result;
    }

}
