<?php

namespace Picalytics\Ibotal\Proxy;

class Proxy {
    
    /**
     *
     * @var string
     */
    private $host;
    
    /**
     *
     * @var integer
     */
    private $port;
    
    /**
     *
     * @var string
     */
    private $login;
    
    /**
     *
     * @var string
     */
    private $pass;
    
    /**
     * 
     * @param string $host
     * @param int $port
     * @param string $login
     * @param string $pass
     */
    public function __construct(string $host, int $port, string $login, string $pass) {
        $this->host = $host;
        $this->port = $port;
        $this->login = $login;
        $this->pass = $pass;
    }
    
    /**
     * 
     * @param string $host
     * @return \Picalytics\Ibotal\Proxy\Proxy
     */
    public function setHost(string $host): Proxy {
        $this->host = $host;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getHost() {
        return $this->host;
    }
    
    /**
     * 
     * @param int $port
     * @return \Picalytics\Ibotal\Proxy\Proxy
     */
    public function setPort(int $port): Proxy {
        $this->port = $port;
        return $this;
    }
    
    /**
     * 
     * @return integer
     */
    public function getPort() {
        return $this->port;
    }
    
    /**
     * 
     * @param string $login
     * @return \Picalytics\Ibotal\Proxy\Proxy
     */
    public function setLogin(string $login): Proxy {
        $this->login = $login;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getLogin() {
        return $this->login;
    }
    
    /**
     * 
     * @param string $pass
     * @return \Picalytics\Ibotal\Proxy\Proxy
     */
    public function setPass(string $pass): Proxy {
        $this->pass = $pass;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getPass() {
        return $this->pass;
    }
    
    /**
     * 
     * @return string
     */
    public function toFormat(): string {
        if (empty($this->login) || empty($this->pass)) {
            return $this->host . ':' . $this->port;
        }
        return $this->login . ':' . $this->pass . '@' . $this->host . ':' . $this->port;
    }
    
}
