<?php

namespace Picalytics\Ibotal\Email\Driver\MailRu;

use Picalytics\Ibotal\Email\Driver\MailInterface;
use Picalytics\Ibotal\Email\Result\Result;

class MailRuService implements MailInterface
{

    private $connection;

    /**
     * @param string $login
     * @param string $pass
     * @return Result|bool
     */
    public function getInstaCode(string $login, string $pass) {
        $this->connect($login, $pass, true);
        
        $result = $this->createResult();

        try {
            $messagesCount = imap_num_msg($this->connection);
        } catch (\Exception $e) {
            return false;
        }

        $neededEmailFind = false;
        while ($messagesCount > 0 && $neededEmailFind === false) {
            $header = imap_header($this->connection, $messagesCount);
            $subject = $header->subject;
            $messageId = $header->message_id;
            $fromInfo = $header->from[0];
            $fromHost = $fromInfo->host;
            $emailUid = imap_uid($this->connection, $messagesCount);

            if ($fromHost === 'mail.instagram.com') {
                $neededEmailFind = true;
            }

            --$messagesCount;
        }

        $body = $this->getPart($emailUid, "TEXT/HTML");
        preg_match('/<font\ssize="\d">(\d+)<\/font>/', $body, $matches);
        if (!isset($matches[1])) {
            return false;
            //throw new \InstaBotLogin\Mail\Exception\MailException('Can\'t parse e-mail with instagram code');
        }
        $instaCode = $matches[1];

        if (strlen($instaCode) !== 6) {
            return false;
            //throw new \InstaBotLogin\Mail\Exception\MailException('Invalid instagram code');
        }
        imap_close($this->connection);

        //$this->log('Instagram code: ' . $instaCode);
        
        $result->success(true);
        $result->instaCode = $instaCode;
        
        return $result;
    }

    /**
     * @param string $login
     * @param string $pass
     * @param bool $force
     * @return bool|resource
     */
    private function connect(string $login, string $pass, bool $force = false) {
        try {
            (is_null($this->connection) || $force) && $this->connection = imap_open('{imap.mail.ru:993/imap/ssl}', $login, $pass);
        } catch(\Exception $e) {
            return false;
        }

        /*if (!$this->connection) {
            return false;
            //throw new \InstaBotLogin\Mail\Exception\MailException('Can\'t connect to mail.ru: ' . imap_last_error());
        }*/

        return $this->connection ?? false;
    }

    /**
     * 
     * @param int $uid
     * @param string $mimeType
     * @param mixed $structure
     * @param int|bool $partNumber
     * @return string
     */
    private function getPart(int $uid, string $mimeType, $structure = false, $partNumber = false): string {
        if (!$structure) {
            $structure = imap_fetchstructure($this->connection, $uid, FT_UID);
        }
        if ($structure) {
            if ($mimeType == $this->getMimeType($structure)) {
                if (!$partNumber) {
                    $partNumber = 1;
                }
                $text = imap_fetchbody($this->connection, $uid, $partNumber, FT_UID);
                switch ($structure->encoding) {
                    case 3:
                        return imap_base64($text);
                    case 4:
                        return imap_qprint($text);
                    default:
                        return $text;
                }
            }

            // multipart 
            if ($structure->type == 1) {
                foreach ($structure->parts as $index => $subStruct) {
                    $prefix = "";
                    if ($partNumber) {
                        $prefix = $partNumber . ".";
                    }
                    $data = $this->getPart($uid, $mimeType, $subStruct, $prefix . ($index + 1));
                    if ($data) {
                        return $data;
                    }
                }
            }
        }
        return '';
    }

    /**
     * 
     * @param object $structure
     * @return string
     */
    private function getMimeType($structure): string {
        $primaryMimetype = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

        if ($structure->subtype) {
            return $primaryMimetype[(int) $structure->type] . "/" . $structure->subtype;
        }
        return "TEXT/PLAIN";
    }

    /**
     * @return Result
     */
    private function createResult(): Result {
        return new Result();
    }

}
