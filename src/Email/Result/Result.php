<?php

namespace Picalytics\Ibotal\Email\Result;

class Result implements ResultInterface
{
    
    /**
     *
     * @var bool
     */
    protected $success = false;
    
    /**
     *
     * @var array
     */
    protected $data = [];

    /**
     * 
     * @param bool $success
     * @return bool
     */
    public function success(bool $success = null): bool
    {
        if (!is_null($success)) {
            $this->success = $success;
        }
        return $this->success;
    }

    /**
     * @param string $name
     * @param $value
     * @return Result
     */
    public function __set(string $name, $value): self
    {
        $this->data[$name] = $value;
        return $this;
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return null;
    }
    
}
