<?php

namespace Picalytics\Ibotal\Email\Result;

interface ResultInterface
{
    
    /**
     * @param bool $success
     * @return bool
     */
    public function success(bool $success = null): bool;
    
}
