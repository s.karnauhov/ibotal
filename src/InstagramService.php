<?php

namespace Picalytics\Ibotal;

class InstagramService {
    
    /**
     *
     * @var InstagramClient
     */
    protected $instagramClient;

    /**
     *
     * @var Proxy\Proxy
     */
    protected $proxy;
    
    /**
     *
     * @var Account\Account 
     */
    protected $account;
    
    /**
     *
     * @var array
     */
    protected $config;

    /**
     * 
     * @param array $config
     */
    public function __construct(array $config) {
        $this->config = $config;
    }

    /**
     * 
     * @param \Picalytics\Ibotal\Proxy\Proxy $proxy
     * @return \Picalytics\Ibotal\InstagramService
     */
    public function setProxy(Proxy\Proxy $proxy): InstagramService {
        $this->proxy = $proxy;
        return $this;
    }
    
    /**
     * 
     * @return \Picalytics\Ibotal\Proxy\Proxy
     */
    public function getProxy(): Proxy\Proxy {
        return $this->proxy;
    }
    
    /**
     * 
     * @param \Picalytics\Ibotal\Account\Account $account
     * @return \Picalytics\Ibotal\InstagramService
     */
    public function setAccount(Account\Account $account): InstagramService {
        $this->account = $account;
        return $this;
    }
    
    /**
     * 
     * @return \Picalytics\Ibotal\Account\Account
     */
    public function getAccount(): Account\Account {
        return $this->account;
    }

    /**
     * 
     * @return \Picalytics\Ibotal\Result
     */
    public function tryToLogin(): Result {
        $result = $this->getResult();
        $this->trashSessionsFiles($this->account->getLogin());
        try {
            $this->getInstagramClient()->login($this->account->getLogin(), $this->account->getPass());
        } catch(\Exception $e) {
            $response = $e->getResponse();
            if ($e instanceof \InstagramAPI\Exception\ChallengeRequiredException && $response->getErrorType() === 'checkpoint_challenge_required') {
                $challengeRequiredUrl = substr($response->getChallenge()->getApiPath(), 1);
                $this->getInstagramClient()->sendVerificationCode($challengeRequiredUrl, 1);
                $result->setError(Result::CHALLENGE_REQUIRED_ERROR);
                $result->setData([
                    'challengeRequiredUrl' => $challengeRequiredUrl,
                ]);
            }
            return $result;
        }
        try {
            $accountInfo = $this->getInstagramClient()->people->getInfoByName($this->account->getLogin());
        } catch (\Exception $e) {
            return $result;
        }
        $result->status(true);
        $result->setError(Result::NO_ERROR);
        return $result;
    }
    
    /**
     * 
     * @param string $challengeRequiredUrl
     * @return bool
     */
    public function challengeRequiredLogin(string $challengeRequiredUrl): bool {
        $this->getInstagramClient()->changeUser($this->account->getLogin(), $this->account->getPass());
        $mailService = new Email\Driver\MailRu\MailRuService();
        $codeResult = $mailService->getInstaCode($this->account->getEmail()->getEmail(), $this->account->getEmail()->getPass());
        if (!($codeResult instanceof Email\Result\Result) || !$codeResult->success()) {
            return false;
        }
        $code = $codeResult->instaCode;
        $this->getInstagramClient()->loginByVerificationCode($challengeRequiredUrl, $code);
        $accountInfo = $this->getInstagramClient()->people->getInfoByName($this->account->getLogin());
        
        return true;
    }

    /**
     * 
     * @return \Picalytics\Ibotal\InstagramClient
     */
    protected function  getInstagramClient(): InstagramClient {
        if (is_null($this->instagramClient)) {
            InstagramClient::$allowDangerousWebUsageAtMyOwnRisk = true;
            $this->instagramClient = new InstagramClient(false, false, $this->config);
            $this->instagramClient->setProxy($this->proxy->toFormat());
        }
        return $this->instagramClient;
    }
    
    /**
     * 
     * @param string $accountName
     */
    protected function trashSessionsFiles(string $accountName) {
        $accountSessionPath = $this->config['basefolder'] . $accountName;
        if (file_exists($accountSessionPath)) {
            foreach (new \DirectoryIterator($accountSessionPath) as $fileInfo) {
                if (!$fileInfo->isDot()) {
                    unlink($fileInfo->getPathname());
                }
            }
            rmdir($accountSessionPath);
        }
    }

    /**
     * 
     * @return \Picalytics\Ibotal\Result
     */
    protected function getResult() {
        return new Result();
    }
    
}
