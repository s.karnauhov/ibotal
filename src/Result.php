<?php

namespace Picalytics\Ibotal;

class Result {
    
    const NO_ERROR = -1;
    const UNKNOWN_ERROR = 0;
    const CHALLENGE_REQUIRED_ERROR = 1;
    
    /**
     *
     * @var bool
     */
    protected $status = false;
    
    /**
     *
     * @var int
     */
    protected $error = self::UNKNOWN_ERROR;
    
    /**
     *
     * @var mixed
     */
    protected $data;
    
    /**
     * 
     * @param bool|null $status
     * @return bool
     */
    public function status(bool $status = null): bool {
        if (!is_null($status)) {
            $this->status = $status;
        }
        return $this->status;
    }
    
    /**
     * 
     * @param int $error
     * @return \Picalytics\Ibotal\Result
     */
    public function setError(int $error): Result {
        $this->error = $error;
        return $this;
    }
    
    /**
     * 
     * @return int
     */
    public function getError(): int {
        return $this->error;
    }
    
    /**
     * 
     * @param mixed $data
     * @return \Picalytics\Ibotal\Result
     */
    public function setData($data): Result {
        $this->data = $data;
        return $this;
    }
    
    /**
     * 
     * @return mixed
     */
    public function getData() {
        return $this->data;
    }
    
}
